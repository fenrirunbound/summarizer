.PHONY: docker-build
docker-build:
	docker build -t slikshooz/summarizer:latest .

.PHONY: docker-run
docker-run:
	docker run --rm -ti slikshooz/summarizer:latest

venv: venv/bin/activate
venv/bin/activate: requirements.txt
	test -d venv || virtualenv venv
	. venv/bin/activate; pip install -Ur requirements.txt
	touch venv/bin/activate
