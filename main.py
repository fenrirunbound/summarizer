#!/usr/bin/env python3

import click
import os

from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser

from sumy.summarizers.edmundson import EdmundsonSummarizer
from sumy.summarizers.kl import KLSummarizer
from sumy.summarizers.lex_rank import LexRankSummarizer
from sumy.summarizers.lsa import LsaSummarizer
from sumy.summarizers.luhn import LuhnSummarizer
from sumy.summarizers.sum_basic import SumBasicSummarizer
from sumy.summarizers.text_rank import TextRankSummarizer
from sumy.utils import get_stop_words

LANGUAGE='english'

class State(object):
    def __init__(self):
        self.lines = 7
        self.alg = "lsa"

pass_state = click.make_pass_decorator(State, ensure=True)

def lines_option(f):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        state.lines = value
        return value
    return click.option('-l', '--lines',
                        default=7,
                        expose_value=False,
                        help='Number of lines to summarize the content to. Defaults: 7.',
                        callback=callback)(f)

def alg_option(f):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        state.alg = value
        return value
    return click.option('-a', '--alg',
                        default='lsa',
                        expose_value=False,
                        type=click.Choice(['edmundson', 'kl', 'lex-rank', 'lsa', 'luhn', 'sum-basic', 'text-rank'], case_sensitive=False),
                        help='Algorithm to summarize with. Default: lsa.',
                        callback=callback)(f)

def common_options(f):
    f = lines_option(f)
    f = alg_option(f)
    return f

def get_summarizer(name, stemmer, parser):
    switcher = {
        'edmundson': EdmundsonSummarizer,
        'kl': KLSummarizer,
        'lexrank': LexRankSummarizer,
        'lex-rank': LexRankSummarizer,
        'lex_rank': LexRankSummarizer,
        'lsa': LsaSummarizer,
        'luhn': LuhnSummarizer,
        'sumbasic': SumBasicSummarizer,
        'sum-basic': SumBasicSummarizer,
        'sum_basic': SumBasicSummarizer,
        'text-rank': TextRankSummarizer,
        'text-rank': TextRankSummarizer,
        'text_rank': TextRankSummarizer,
    }
    s = switcher.get(name, "lsa")
    summarizer = s(stemmer)
    if name == 'edmundson':
        summarizer.null_words = get_stop_words(LANGUAGE)
        summarizer.bonus_words = parser.significant_words
        summarizer.stigma_words = parser.stigma_words

    return summarizer

@click.group()
def main():
    """
    CLI tool to summarize text
    """
    pass

@main.command()
@click.argument('filepath')
@common_options
@pass_state
def from_file(state, filepath):
    """
    from_file summarizes the content within the specified file
    """
    full_path = os.path.abspath(filepath)
    parser = PlaintextParser.from_file(full_path, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)

    summarizer = get_summarizer(name=state.alg, stemmer=stemmer, parser=parser)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    for sentence in summarizer(parser.document, state.lines):
        click.echo('> %s' % sentence)

@main.command()
@click.argument('url')
@common_options
@pass_state
def from_url(state, url):
    """
    from_url summarizes the content from the provided URL
    """
    parser = HtmlParser.from_url(url, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)

    summarizer = get_summarizer(name=state.alg, stemmer=stemmer, parser=parser)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    for sentence in summarizer(parser.document, state.lines):
        click.echo('> %s' % sentence)

if __name__ == "__main__":
    main()
